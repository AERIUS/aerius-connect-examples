Dit project bevat een aantal programmeer voorbeelden voor het gebruik van AERIUS Connect. Deze voorbeelden zijn puur ter illustratie van het gebruik van de AERIUS Connect API.

**Let op! Vanaf API v3 is websocket vervangen door HTTP REST als communicatie protocol.**

Het hier geleverde voorbeeld script is gebouwd met Python. Om het script te kunnen gebruiken is Python **3.5** nodig.

## Voor Windows gebruikers:
Om de module bravado te installeren is de juiste versie van Microsoft Visual Studio nodig. Dit kan eenvoudig als volgt geinstalleerd worden:

### Stap 1:
Download en installeer python 3.5 van https://www.python.org/downloads/

### Stap 2:
<code>pip install --upgrade setuptools</code>
(Het programma <code>pip</code> staat in the python installatie directory <code>Scripts</code>)

### Stap 3:
Download en installeer de Microsoft VisualStudio C++ Build Tools 2015 van: http://landinghub.visualstudio.com/visual-cpp-build-tools

### Stap 4:
Installeer de voor het script specifieke modules:
- module arrow: <code>pip install arrow</code>
- module bravado: <code>pip install bravado</code>

Hierna kan het script gebruikt worden.
<code>python AERIUS-Connect.py --help</code>

## Voor ArcGIS gebruikers:
Bij ArcGIS wordt python 2 meegeleverd. Deze versie is niet geschikt voor gebruik met het AERIUS script. Er wordt daarom geadviseerd om python 3.5 apart te installeren.

## python 2
Door een probleem in python 2 is het niet mogelijk om het AERIUS Script te zijn gebruiken om grote bestanden te versturen. De oplossing is om python 3.5 te gebruiken.


